import gzip
import json

import numpy as np
from path import Path

from ds_ua.regexes.rnn import EMAIL_DOMAIN, MULTIPLE_WHITESPACES, NON_ALPHA

RNN_PARAMETERS = Path('ds_ua/parameters/params.json')
RNN_TRAIN_PATHS = Path('data/train_ner')



def preprocess_text(text):
    text = NON_ALPHA.sub(' ', text)
    text = MULTIPLE_WHITESPACES.sub(' ', text)
    text = EMAIL_DOMAIN.sub(' ', text)

    text = text.lower().strip()

    return text


def load_parameters(rnn_parameters=RNN_PARAMETERS, rnn='attention_rnn'):
    with rnn_parameters.open(mode='rt') as fp:
        parameters = json.load(fp)

    return parameters[rnn]


def load_vocab(rnn_train_paths=RNN_TRAIN_PATHS):
    vocab_path = rnn_train_paths / 'vocab.json'

    with vocab_path.open(mode='rt') as fp:
        words_vocab = json.load(fp)

    return words_vocab


def load_sentences(rnn_train_paths=RNN_TRAIN_PATHS):
    sentences_path = rnn_train_paths / 'sentences.json'

    with sentences_path.open(mode='rt') as fp:
        sentences = json.load(fp)

    return sentences


def load_labels(rnn_train_paths=RNN_TRAIN_PATHS):
    labels_path = rnn_train_paths / 'labels.json'

    with labels_path.open(mode='rt') as fp:
        labels = json.load(fp)

    return labels


def load_embeddings(rnn_train_paths=RNN_TRAIN_PATHS):
    embeddings_path = rnn_train_paths / 'embeddings.npz'
    embeddings = np.load(embeddings_path)

    return embeddings['embeddings']


def pad_sequence(sentence, padding_len, padding_sym):
    if len(sentence) == padding_len:
        return sentence, padding_len

    padding = [padding_sym] * (padding_len - len(sentence))
    sentence += padding

    return sentence, min(len(sentence), padding_len)


def minibatches(data, labels, minibatch_size):
    x_batch, y_batch = [], []

    for (x, y) in zip(data, labels):
        if len(x_batch) == minibatch_size:
            yield x_batch, y_batch
            x_batch, y_batch = [], []

        x_batch.append(x)
        y_batch.append(y)

    if len(x_batch) != 0:
        yield x_batch, y_batch


def encode_data(sentences, labels, words_vocab, sentence_max_len, padding_sym, allow_unknown=True):
    encoded_tokens, encoded_labels, sequence_lengths = [], [], []

    for ix, sentence in enumerate(sentences):
        encoded_sentence = []

        for word in sentence:
            if words_vocab.get(word):
                encoded_sentence.append(words_vocab[word])

            if allow_unknown:
                encoded_sentence.append(words_vocab['<unk>'])
            else:
                raise KeyError('Unknown token is not allowed')

        if len(encoded_sentence) > sentence_max_len:
            encoded_sentence = encoded_sentence[:sentence_max_len]
            sequence_length = sentence_max_len

        else:
            (encoded_sentence, sequence_length) = pad_sequence(
                encoded_sentence,
                sentence_max_len,
                padding_sym,
            )

        encoded_tokens.append(encoded_sentence)
        encoded_labels.append(int(labels[ix]))
        sequence_lengths.append(sequence_length)

    return encoded_tokens, encoded_labels, sequence_lengths

import argparse
import gzip
import json
from collections import Counter

import numpy as np
import pandas as pd
import spacy
from path import Path

from ds_ua.utils.preprocessors import load_parameters, preprocess_text

glove_embeddings = Path('data/train_ner/glove_embeddings.json.gz')
nlp = spacy.load('en_core_web_sm')


def load_embeddings():
    """
    Load pre-defined GloVe word embeddings
    
    Returns:
        dictionary of pre-trained word embeddings

    """
    with gzip.open(glove_embeddings, mode='r') as fp:
        embeddings = json.load(fp)

    return embeddings


def clean_sentences(data):
    """
    Clean sentences from punctuation, special symbols, etc.
    
    Args:
        data: pandas dataframe containing sentences and their labels

    Returns:
        nested list of preprocessed sentences

    """
    cleaned_sentences = []

    sentences = data['sentence']

    for sentence in sentences:
        cleaned = [word for word in preprocess_text(sentence).split()]
        cleaned_sentences.append(cleaned)

    return cleaned_sentences


def words2vocab(tokenized_sentences):
    """
    Transform nested list of tokenized words to a vocabulary
    
    Args:
        tokenized_sentences: nested list of tokenized words

    Returns:
        vocabulary of tokenized words

    """
    parameters = load_parameters()

    words_vocab = dict()
    words_count = Counter(tokenized_sentences).most_common()
    
    words_vocab['<padding>'] = parameters['padding_sym']
    words_vocab['<unk>'] = parameters['unknown_sym']

    for word, _ in words_count:
        words_vocab[word] = len(words_vocab)

    return words_vocab


def create_vocab(data):
    """
    Create a vocabulary of tokenized words from pandas dataframe
    containing sentences and their labels

    Args:
        data: pandas dataframe containing sentences and their labels

    Returns:
        vocabulary of tokenized words

    """
    cleaned_sentences = []
    cleaned_words = []

    sentences = data['sentence']

    for sentence in sentences:
        doc = nlp(sentence)

        for token in doc:
            cleaned_sentences.append(token.text)

    words_vocab = words2vocab(cleaned_sentences)

    return words_vocab


def tokens2embeddings(data):
    """
    Create numpy array of GloVe embeddings, which correspond to the
    tokenized words, found in the pandas dataframe containing
    sentences and their labels

    Args:
        data: pandas dataframe containing sentences and their labels

    Returns:
        nested list of GloVe embeddings, which is sorted in the order,
        which corresponds to the order of the tokens in the vocabulary

    """
    parameters = load_parameters()
    embeddings = load_embeddings()

    words_vocab = create_vocab(data)

    tokens_embeddings = []
    inversed_words_vocab = dict(zip(words_vocab.values(), words_vocab.keys()))

    for _, word in sorted(inversed_words_vocab.items()):
        if word in embeddings:
            tokens_embeddings.append(embeddings.get(word))

        else:
            tokens_embeddings.append([0 for ix in range(parameters['embedding_size'])])

    tokens_embeddings = np.array(tokens_embeddings)

    return tokens_embeddings


def data2cleaned_sentences(data, write_path):
    """
    Write preprocessed sentences to a .json file
    
    Args:
        data: pandas dataframe containing sentences and their labels
        write_path: path of the target .json file

    """
    cleaned_sentences = clean_sentences(data)

    with Path(write_path).open(mode='wt') as fp:
        json.dump(cleaned_sentences, fp)


def data2vocab(data, write_path):
    """
    Write a vocabulary of tokenized words to a .json file

    Args:
        data: pandas dataframe containing sentences and their labels
        write_path: path of the target .json file

    """
    words_vocab = create_vocab(data)

    with Path(write_path).open(mode='wt') as fp:
        json.dump(words_vocab, fp)


def data2embeddings(data, write_path):
    """
    Write a numpy array of GloVe embeddings to a .npz file

    Args:
        data: pandas dataframe containing sentences and their labels
        write_path: path of the target .npz file
    
    """
    tokens_embeddings = tokens2embeddings(data)
    np.savez_compressed(write_path, embeddings=tokens_embeddings)


def data2labels(data, write_path):
    """
    Write binary labels to a .json file

    Args:
        data: pandas dataframe containing sentences and their labels
        write_path: path of the target .json file

    """
    labels = [str(label) for label in data['label']]

    with Path(write_path).open(mode='wt') as fp:
        json.dump(labels, fp)   
    

def form_write_paths(write_dir_path):
    """
    Create paths to write the results to

    Args:
        write_dir_path: base directory to write the results to

    Return:
        paths to write labels, sentences, vocabulary and embeddings to

    """
    write_path_labels = write_dir_path + '/labels.json'
    write_path_sentences = write_dir_path + '/sentences.json'
    write_path_vocab = write_dir_path + '/vocab.json'
    write_path_embeddings = write_dir_path + '/embeddings.npz'

    return write_path_labels, write_path_sentences, write_path_vocab, write_path_embeddings


if __name__ == '__main__':
    parser = argparse.ArgumentParser()

    parser.add_argument('read_path', type=str)
    parser.add_argument('write_dir_path', type=str)

    args = parser.parse_args()

    data = pd.read_csv(args.read_path, delimiter='\t')

    write_path_labels, write_path_sentences, write_path_vocab, write_path_embeddings = form_write_paths(
        args.write_dir_path
    )

    data2labels(data, write_path_labels)
    data2cleaned_sentences(data, write_path_sentences)
    data2vocab(data, write_path_vocab)
    data2embeddings(data, write_path_embeddings)

import tensorflow as tf


def add_word_embeddings_schema(
        embeddings,
        word_ids,
        vocabulary_size,
        embeddings_size,
        use_glove=True
    ):
    """
    Load word embeddings to a tensor and perform lookup of the word ids on them

    Args:
        embeddings: numpy array of the word embeddings (sorted in the order,
            which corresponds to the order of the tokens in the vocabulary)
        word_ids: nested list of words ids
        vocabulary_size: number of tokens in the vocabulary
        embeddings_size: size of each of the embeddings
        use_glove: indicator whether to use GloVe embeddings or create
            embeddings ourselves

    Return:
        mapped lookup of the embeddings to the word ids

    """
    with tf.name_scope('embedding'):
        if use_glove:
            word_embeddings = tf.constant(embeddings, dtype=tf.float32, shape=[len(embeddings), embeddings_size])
            word_embeddings = tf.get_variable('embeddings', initializer=word_embeddings, trainable=False)

        else:
            word_embeddings = tf.random_uniform([vocabulary_size, embeddings_size])
            word_embeddings = tf.get_variable('embeddings', initializer=word_embeddings, trainable=True)

        mapped_embeddings = tf.nn.embedding_lookup(word_embeddings, word_ids)

    return mapped_embeddings


def add_bi_rnn_logits(
        word_embeddings,
        sequence_length,
        num_hidden,
        num_layers,
        keep_prob,
    ):
    """
    Create Bi-RNN architecture of the neural network with forward and backward cells,
    which are stacked

    Args:
        word_embeddings: mapped lookup of the word embeddings
        sequence_lengths: lengths of the sequences
        num_hidden: number of hidden units
        num_layers: number of hidden layers
        keep_prob: keep probability parameter

    Returns:
        stacked architecture of the bidirectional dynamic RNN

    """
    with tf.name_scope('bi_rnn'):
        fw_cells = [tf.nn.rnn_cell.LSTMCell(num_hidden) for _ in range(num_layers)]
        bw_cells = [tf.nn.rnn_cell.LSTMCell(num_hidden) for _ in range(num_layers)]

        fw_cells = [tf.nn.rnn_cell.DropoutWrapper(cell, output_keep_prob=keep_prob) for cell in fw_cells]
        bw_cells = [tf.nn.rnn_cell.DropoutWrapper(cell, output_keep_prob=keep_prob) for cell in bw_cells]

        rnn_outputs, _, _ = tf.contrib.rnn.stack_bidirectional_dynamic_rnn(
            fw_cells, bw_cells, word_embeddings, sequence_length=sequence_length, dtype=tf.float32)

    return rnn_outputs


def add_attention_layer(rnn_outputs):
    """
    Add the attention layer

    Args:
        rnn_outputs: architecture of the bidirectional dynamic RNN

    Returns:
        Attention score and output of the layer

    """
    with tf.name_scope('attention'):
        attention_score = tf.nn.softmax(tf.contrib.slim.fully_connected(rnn_outputs, 1))
        attention_output = tf.squeeze(
            tf.matmul(tf.transpose(rnn_outputs, perm=[0, 2, 1]), attention_score),
            axis=-1,
        )

    return attention_score, attention_output


def add_activation_layer(attention_output, num_classes):
    """
    Add the activation layer on top of the network

    Args:
        attention_output: output of the attention layer
        num_classes: number of labels to make predictions for

    Returns:
        logits and predictions on the output layer of the neural network

    """
    with tf.name_scope('output'):
        activation_logits = tf.contrib.slim.fully_connected(attention_output, num_classes, activation_fn=None)
        predictions = tf.argmax(activation_logits, -1, output_type=tf.int32)

    return activation_logits, predictions


def add_loss_function(
        activation_logits,
        labels,
        learning_rate,
        global_step,
    ):
    """
    Add the loss function to learn the model with

    Args:
        activation_logits: activation logits on the output layer of the neural network
        labels: target labels to learn with
        learning_rate: learning rate to train model with
        global_step: global step of the model

    Returns:
        train loss and the optimizer of the model (default is Adam optimizer)

    """
    with tf.name_scope('loss'):
        loss = tf.reduce_mean(
            tf.nn.sparse_softmax_cross_entropy_with_logits(logits=activation_logits, labels=labels)
        )

        optimizer = tf.train.AdamOptimizer(learning_rate).minimize(loss, global_step=global_step)

    return loss, optimizer


def calculate_accuracy(predictions, labels):
    """
    Calculate the accuracy of the model

    Args:
        predictions: predictions made by the model
        labels: true labels
    
    Returns:
        accuracy value

    """
    correct_predictions = tf.equal(predictions, labels)
    accuracy = tf.reduce_mean(tf.cast(correct_predictions, 'float'), name='accuracy')

    return accuracy

import numpy as np
import tensorflow as tf

from ds_ua.utils.preprocessors import (encode_data, load_embeddings,
                                       load_labels, load_parameters,
                                       load_sentences, load_vocab, minibatches,
                                       preprocess_text)
from ds_ua.utils.schema import (add_activation_layer, add_attention_layer,
                                add_bi_rnn_logits, add_loss_function,
                                add_word_embeddings_schema, calculate_accuracy)


class RNNTrainer:
    def __init__(self):
        self.parameters = load_parameters()

    def load_data(self):
        self.words_vocab = load_vocab()
        self.sentences = load_sentences()
        self.labels = load_labels()
        self.embeddings = load_embeddings()

    def add_counts(self):
        self.vocabulary_size = len(self.words_vocab)

    def add_placeholders(self):
        self.tf_labels = tf.placeholder(
            tf.int32,
            shape=[None],
            name='tf_labels',
        )
        self.tf_word_ids = tf.placeholder(
            tf.int32,
            [None, self.parameters['sentence_max_len']],
            name='tf_word_ids',
        )

        self.tf_word_ids_sequence_lengths = tf.reduce_sum(
            tf.sign(self.tf_word_ids),
            1,
        )
        self.tf_global_step = tf.Variable(
            0,
            trainable=False,
        )
        self.tf_keep_prob = tf.placeholder(
            tf.float32,
            [],
            name='keep_prob',
        )
        self.global_step = tf.Variable(0, trainable=False)

    def get_feed_dict(
        self,
        encoded_words,
        keep_prob,
        labels=None,
        train=True
    ):
        feed = dict()

        feed[self.tf_word_ids] = encoded_words
        feed[self.tf_keep_prob] = keep_prob

        if train:
            feed[self.tf_labels] = labels

        return feed

    def initialize_training_schema(self):
        self.word_embeddings = add_word_embeddings_schema(
            self.embeddings,
            self.tf_word_ids,
            self.vocabulary_size,
            self.parameters['embedding_size'],
        )
        self.bi_rnn_logits = add_bi_rnn_logits(
            self.word_embeddings,
            self.tf_word_ids_sequence_lengths,
            self.parameters['num_hidden'],
            self.parameters['num_layers'],
            self.parameters['keep_prob'],
        )
        self.attention_score, self.attention_output = add_attention_layer(
            self.bi_rnn_logits,
        )

        self.logits, self.predictions = add_activation_layer(
            self.attention_output,
            self.parameters['num_classes'],
        )
        self.loss, self.optimizer = add_loss_function(
            self.logits,
            self.tf_labels,
            self.parameters['learning_rate'],
            self.global_step,
        )
        
        self.accuracy = calculate_accuracy(self.predictions, self.tf_labels)

    def predict_batch(self, encoded_words):
        feed_dict = self.get_feed_dict(encoded_words, self.parameters['keep_prob'], train=False)
        predictions = self.sess.run(self.predictions, feed_dict=feed_dict)

        return predictions

    def run_epoch(self, encoded_words, encoded_labels, keep_prob=0.8):
        losses = []

        for tokens, labels in minibatches(encoded_words, encoded_labels, self.parameters['minibatch_size']):
            feed_dict = self.get_feed_dict(tokens, keep_prob, labels)
            _, step, loss = self.sess.run([self.optimizer, self.global_step, self.loss], feed_dict=feed_dict)

            print('Loss: {0}'.format(loss))

            losses.append(loss)

        print('Averaged loss for the epoch: {0}\n'.format(np.mean(losses)))
        return np.mean(losses)

    def build_classifier(self, encoded_words, encoded_tags):
        best_score = 0
        num_epochs_no_improve = 0

        for epoch in range(self.parameters['num_epochs']):
            print('Running epoch {0}...'.format(epoch))
            score = self.run_epoch(encoded_words, encoded_tags, self.parameters['keep_prob'])

        print('Loss after {0} epochs: {1}'.format(self.parameters['num_epochs'], score))

    def add_parameters(self):
        self.add_placeholders()
        self.add_counts()

    def create_session(self):
        self.sess = tf.Session()
        self.sess.run(tf.global_variables_initializer())
        self.saver = tf.train.Saver()

    def initialize_session(self):
        self.load_data()
        self.add_parameters()
        self.initialize_training_schema()
        self.create_session()

    def train(self):
        self.initialize_session()

        encoded_words, encoded_labels, sequence_lengths = encode_data(
            self.sentences,
            self.labels,
            self.words_vocab,
            self.parameters['sentence_max_len'],
            self.parameters['padding_sym'],
        )
        self.build_classifier(encoded_words, encoded_labels)

t = RNNTrainer()
t.train()

import regex


NON_ALPHA = regex.compile('[^A-Za-z0-9(),!?\'\\`\"]')
MULTIPLE_WHITESPACES = regex.compile('\\s{2,}')
EMAIL_DOMAIN = regex.compile('@[A-Za-z0-9]+')

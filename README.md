## DS UA workshop

### Installation

0. recommendation is to firstly create pyenv and use it for the repo (if having troubles with activation then `eval "$(pyenv init -)"`)
1. install dependencies with `pip install -r requirements.txt`
2. set up PYTHONPATH `export PYTHONPATH="path/to/the/root/of/the/repository"`
3. set up environmental variables `export LC_ALL=en_US.UTF-8` and `export LANG=en_US.UTF-8`
4. set up spacy model `python -m spacy download en`
